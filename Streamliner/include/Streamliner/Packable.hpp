#ifndef STLR_PACKABLE_HPP
#define STLR_PACKABLE_HPP

#include <memory>
#include <SFML/Network/Packet.hpp>

typedef unsigned short PackableTypeID;

namespace stlr
{
    class Packable
    {
    private:
        PackableTypeID m_typeid;
    protected:
        Packable(PackableTypeID TypeID);
        virtual void OnPack(sf::Packet& Packet) = 0;
        virtual void OnUnpack(sf::Packet& Packet) = 0;
    public:
        virtual ~Packable() = default;
        void Pack(sf::Packet& Packet);
        void Unpack(sf::Packet& Packet);
        sf::Packet& operator >>(sf::Packet& Packet);
        sf::Packet& operator <<(sf::Packet& Packet);
    };
}

typedef std::unique_ptr<stlr::Packable> PackablePtr;

#endif
