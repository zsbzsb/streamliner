#ifndef STLR_CLIENTAUTHORIZEWORKER_HPP
#define STLR_CLIENTAUTHORIZEWORKER_HPP

#include <vector>
#include <memory>
#include <functional>
#include <SFML/System/Time.hpp>
#include <SFML/System/Clock.hpp>
#include <SFML/Network/Packet.hpp>
#include <Streamliner/Server/JobBase.hpp>
#include <Streamliner/Server/Client.hpp>
#include <Streamliner/Server/ClientPool.hpp>

namespace stlr
{
    class ClientAuthorizeJob : public stlr::JobBase
    {
    private:
        sf::Clock m_updateclock;
        std::vector<ClientPoolPtr> m_sourcepools;
        std::vector<ClientPoolPtr> m_destinationpools;
        std::function<const bool(ClientPtr&)> m_clientauthorizationfunction;
        std::function<void(ClientPtr& Client, const std::vector<ClientPoolPtr>&)> m_assignpoolfunction;
    public:
        ClientAuthorizeJob();
        void RegisterSourcePool(ClientPoolPtr ClientPool);
        void RegisterDestinationPool(ClientPoolPtr ClientPool);
        void Initialize();
        const bool DoesNeedWork();
        void DoWork();
        void SetClientAuthorizationFunction(std::function<const bool(ClientPtr&)> Function);
        void SetAssignPoolFunction(std::function<void(ClientPtr& Client, const std::vector<ClientPoolPtr>&)> Function);
        static const bool DefaultClientAuthorization(ClientPtr& Client);
        static void DefaultAssignPool(ClientPtr& Client, const std::vector<ClientPoolPtr>& Pools);
    };
}

typedef std::shared_ptr<stlr::ClientAuthorizeJob> ClientAuthorizeJobPtr;

#endif
