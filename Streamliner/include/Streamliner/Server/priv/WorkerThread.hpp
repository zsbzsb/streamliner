#ifndef PRIV_WORKERTHREAD_HPP
#define PRIV_WORKERTHREAD_HPP

#include <memory>
#include <thread>
#include <chrono>
#include <SFML/System/NonCopyable.hpp>
#include <Streamliner/Server/JobBase.hpp>
#include <Streamliner/Server/priv/ThreadPoolControl.hpp>

namespace priv
{
    class WorkerThread : private sf::NonCopyable
    {
    private:
        priv::ThreadPoolControl* m_control;
        std::unique_ptr<std::thread> m_thread;
        void ThreadRun();
        JobSharedPtr FindWork(std::queue<JobWeakPtr>& JobQueue);
    public:
        WorkerThread(priv::ThreadPoolControl* Control);
        void Start();
        void Join();
    };
}

typedef std::unique_ptr<priv::WorkerThread> WorkerThreadPtr;

#endif
