#ifndef PRIV_THREADCONTROL_HPP
#define PRIV_THREADCONTROL_HPP

#include <atomic>
#include <queue>
#include <SFML/System/NonCopyable.hpp>
#include <Streamliner/LockableItem.hpp>
#include <Streamliner/Server/JobBase.hpp>

namespace priv
{
    struct ThreadPoolControl : private sf::NonCopyable
    {
        std::atomic_bool ShouldRun;
        std::atomic<unsigned short> RunningCount;
        stlr::LockableItem<std::queue<JobWeakPtr>> JobQueue;
        ThreadPoolControl();
    };
}

#endif
