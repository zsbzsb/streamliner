#ifndef PRIV_WORKERBASE_HPP
#define PRIV_WORKERBASE_HPP

#include <memory>
#include <SFML/System/NonCopyable.hpp>

namespace stlr
{
    class JobBase : private sf::NonCopyable
    {
    public:
        virtual ~JobBase();
        virtual void Initialize();
        virtual const bool DoesNeedWork() = 0;
        virtual void DoWork() = 0;
        virtual void Shutdown();
    };
}

typedef std::shared_ptr<stlr::JobBase> JobSharedPtr;
typedef std::weak_ptr<stlr::JobBase> JobWeakPtr;

#endif
