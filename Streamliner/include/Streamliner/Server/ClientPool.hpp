#ifndef STLR_CLIENTPOOL_HPP
#define STLR_CLIENTPOOL_HPP

#include <atomic>
#include <deque>
#include <memory>
#include <SFML/System/NonCopyable.hpp>
#include <Streamliner/LockableItem.hpp>
#include <Streamliner/Server/Client.hpp>

namespace stlr
{
    class ClientPool : private sf::NonCopyable
    {
    private:
        std::atomic<unsigned short> m_clientcount;
        stlr::LockableItem<std::deque<ClientPtr>> m_clients;
    public:
        ClientPool();
        void AddClient(ClientPtr& Client);
        void RemoveClient(ClientPtr& Client);
        void MoveClient(ClientPtr& Client, std::shared_ptr<stlr::ClientPool>& ClientPool);
        unsigned short GetClientCount();
        stlr::LockableItem<std::deque<ClientPtr>>& GetClients();
    };
}

typedef std::shared_ptr<stlr::ClientPool> ClientPoolPtr;

#endif
