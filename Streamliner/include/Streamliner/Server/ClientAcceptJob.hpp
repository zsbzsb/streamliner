#ifndef STLR_CLIENTACCEPTWORKER_HPP
#define STLR_CLIENTACCEPTWORKER_HPP

#include <vector>
#include <memory>
#include <SFML/Network/TcpListener.hpp>
#include <SFML/System/Time.hpp>
#include <SFML/System/Clock.hpp>
#include <Streamliner/Packable.hpp>
#include <Streamliner/PackableTypes.hpp>
#include <Streamliner/ClientInformation.hpp>
#include <Streamliner/Server/JobBase.hpp>
#include <Streamliner/Server/Client.hpp>
#include <Streamliner/Server/ClientPool.hpp>

namespace stlr
{
    class ClientAcceptJob : public stlr::JobBase
    {
    private:
        sf::TcpListener m_listener;
        unsigned short m_port;
        sf::Clock m_updateclock;
        std::vector<ClientPoolPtr> m_clientpools;
        PackableTypeID m_infotypeid;
    public:
        ClientAcceptJob(unsigned short Port, PackableTypeID ClientInformationTypeID = stlr::ClientInformation::ClientInformationTypeID);
        void RegisterClientPool(ClientPoolPtr ClientPool);
        void Initialize();
        const bool DoesNeedWork();
        void DoWork();
        void Shutdown();
    };
}

typedef std::shared_ptr<stlr::ClientAcceptJob> ClientAcceptJobPtr;

#endif
