#ifndef STLR_SERVER_HPP
#define STLR_SERVER_HPP

#include <memory>
#include <vector>
#include <thread>
#include <chrono>
#include <SFML/System/NonCopyable.hpp>
#include <Streamliner/Server/JobBase.hpp>
#include <Streamliner/Server/priv/ThreadPoolControl.hpp>
#include <Streamliner/Server/priv/WorkerThread.hpp>

namespace stlr
{
    class Server : private sf::NonCopyable
    {
    private:
        priv::ThreadPoolControl m_threadcontrol;
        std::vector<WorkerThreadPtr> m_workerthreads;
        std::vector<JobSharedPtr> m_jobs;
    public:
        Server(unsigned short WorkerCount);
        void RegisterJob(JobSharedPtr& Job);
        void Start();
        void Stop();
    };
}

#endif
