#ifndef STLR_CLIENT_HPP
#define STLR_CLIENT_HPP

#include <map>
#include <deque>
#include <queue>
#include <memory>
#include <iterator>
#include <SFML/System/NonCopyable.hpp>
#include <SFML/Network/TcpSocket.hpp>
#include <SFML/Network/Packet.hpp>
#include <Streamliner/LockableBase.hpp>
#include <Streamliner/Packable.hpp>
#include <Streamliner/PackableTypes.hpp>
#include <Streamliner/ClientInformation.hpp>
#include <Streamliner/Updateable.hpp>

typedef std::unique_ptr<sf::Packet> PacketPtr;
typedef int MappedID;

namespace stlr
{
    class Client : private sf::NonCopyable, public stlr::LockableBase
    {
        typedef unsigned int UpdateableID;
        typedef unsigned char PacketType;
    private:
        sf::TcpSocket m_socket;
        PackablePtr m_information;
        std::unordered_map<UpdateableID, UpdateablePtr> m_localupdateables;
        std::unordered_map<UpdateableID, UpdateablePtr> m_remoteupdateables;
        std::queue<PackablePtr> m_receivedpackables;
        std::queue<PacketPtr> m_custompackets;
        std::unordered_map<MappedID, Updateable*> m_mappedupdateables;
    public:
        Client();
        ~Client();
        sf::TcpSocket& GetSocket();
        const PackablePtr& GetInformation();
        void HandleIncomingData();
        void SendAuthorizedState(bool Authorized);
        const PackablePtr& PeakRecievedPackable();
        const size_t GetRecievedPackableCount();
        void SkipRecievedPackable();
        const bool HandleRecievedPackable(PackablePtr& TransferableOut);
        const PacketPtr& PeakCustomPacket();
        const size_t GetCustomPacketCount();
        void SkipCustomPacket();
        const bool HandleCustomPacket(PacketPtr& PacketOut);
        const size_t GetLocalUpdateablesCount();
        const UpdateablePtr& GetLocalUpdateable(size_t Index);
        const size_t GetRemoteUpdateablesCount();
        const UpdateablePtr& GetRemoteUpdateable(size_t Index);
    };
}

typedef std::shared_ptr<stlr::Client> ClientPtr;

#endif
