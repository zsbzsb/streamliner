#ifndef STLR_UPDATEABLE_HPP
#define STLR_UPDATEABLE_HPP

#include <memory>
#include <SFML/Network/Packet.hpp>
#include <Streamliner/Packable.hpp>

namespace stlr
{
    class Updateable : public stlr::Packable
    {
    private:
        bool m_remoteupdated;
    protected:
        Updateable(PackableTypeID TypeID);
    public:
        virtual ~Updateable() = default;
        const bool RemoteUpdateAvailable();
        void SetRemoteUpdateAvailable();
        void RemoteUpdateHandled();
        virtual const bool LocalChangeAvailable() = 0;
        virtual void LocalChangeHandled() = 0;
    };
}

typedef std::unique_ptr<stlr::Updateable> UpdateablePtr;

#endif
