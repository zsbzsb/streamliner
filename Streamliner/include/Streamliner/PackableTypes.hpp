#ifndef STLR_PACKABLETYPES_HPP
#define STLR_PACKABLETYPES_HPP

#include <memory>
#include <unordered_map>
#include <functional>
#include <Streamliner/Packable.hpp>

namespace stlr
{
    class PackableTypes
    {
    private:
        static std::unordered_map<PackableTypeID, std::function<void(PackablePtr&)>> m_types;
    public:
        static void RegisterType(PackableTypeID TypeID, std::function<void(PackablePtr&)> Contructor);
        static void ConstructPackable(PackableTypeID TypeID, PackablePtr& ConstructedTransferable);
    };
}

#endif
