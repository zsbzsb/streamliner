#ifndef STLR_CLIENTINFORMATION_HPP
#define STLR_CLIENTINFORMATION_HPP

#include <string>
#include <Streamliner/Packable.hpp>
#include <Streamliner/PackableTypes.hpp>

namespace stlr
{
    class ClientInformation : public stlr::Packable
    {
    private:
        std::string m_account;
        std::string m_password;
        std::string m_nickname;
    public:
        static const PackableTypeID ClientInformationTypeID = 0;
        ClientInformation(std::string Account, std::string Password, std::string Nickname, PackableTypeID TypeID = ClientInformationTypeID);
        virtual ~ClientInformation() = default;
        const std::string GetAccount();
        const std::string GetPassword();
        const std::string GetNickname();
        static void RegisterType();
    protected:
        virtual void OnPack(sf::Packet& Packet);
        virtual void OnUnpack(sf::Packet& Packet);
    };
}

#endif
