#ifndef STLR_LOCKABLEBASE_HPP
#define STLR_LOCKABLEBASE_HPP

#include <atomic>
#include <SFML/System/Mutex.hpp>

namespace stlr
{
    class LockableBase
    {
    private:
        sf::Mutex m_lock;
        std::atomic<int> m_count;
    public:
        LockableBase();
        void Lock();
        void Release();
        const bool IsLocked();
    };
}

#endif
