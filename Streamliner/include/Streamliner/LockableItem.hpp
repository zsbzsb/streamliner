#ifndef STLR_LOCKABLEITEM_HPP
#define STLR_LOCKABLEITEM_HPP

#include <memory>
#include <Streamliner/LockableBase.hpp>

namespace stlr
{
    template <typename T>
    class LockableItem : public stlr::LockableBase
    {
    private:
        std::unique_ptr<T> m_value;
    public:
        LockableItem() : stlr::LockableBase(), m_value(new T) { }
        T& LockItem()
        {
            stlr::LockableBase::Lock();
            return *m_value;
        }
        T& GetItem()
        {
            return *m_value;
        }
    };
}

#endif