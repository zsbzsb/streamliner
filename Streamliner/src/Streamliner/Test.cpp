#include <Streamliner/Server/Server.hpp>
#include <Streamliner/Server/ClientAcceptJob.hpp>
#include <Streamliner/Server/ClientAuthorizeJob.hpp>
#include <Streamliner/Server/ClientPool.hpp>
#include <Streamliner/Server/JobBase.hpp>
#include <Streamliner/ClientInformation.hpp>

#include <iostream>

int main()
{
    // create a server with 3 worker threads
    stlr::Server server(3);

    // register the default client info type id
    stlr::ClientInformation::RegisterType();

    // create a pool to accept incoming clients
    ClientPoolPtr acceptpool(new stlr::ClientPool);

    // create a job to accept clients on port 1995
    ClientAcceptJobPtr acceptjob(new stlr::ClientAcceptJob(1995));

    // register the client incoming pool
    acceptjob->RegisterClientPool(acceptpool);

    // register the job with the server
    server.RegisterJob(JobSharedPtr(acceptjob));

    // create a pool for authorized clients
    ClientPoolPtr authorizedpool(new stlr::ClientPool);

    // create a job to authorize clients
    ClientAuthorizeJobPtr authorizejob(new stlr::ClientAuthorizeJob());

    // register the unauthorized client pool
    authorizejob->RegisterSourcePool(acceptpool);

    // register the authorized client pool
    authorizejob->RegisterDestinationPool(authorizedpool);

    // register the job that authorizes clients
    server.RegisterJob(JobSharedPtr(authorizejob));

    // start the server
    server.Start();
    
    // wait for some input
    std::cin.get();

    // stop the server and ensure a clean shutdown
    server.Stop();
}