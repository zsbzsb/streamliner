#ifndef STLR_CLIENTINFORMATION_CPP
#define STLR_CLIENTINFORMATION_CPP

#include <Streamliner/ClientInformation.hpp>

namespace stlr
{
    ClientInformation::ClientInformation(std::string Account, std::string Password, std::string Nickname, PackableTypeID TypeID) : stlr::Packable(TypeID), m_account(Account), m_password(Password), m_nickname(Nickname) { }

    const std::string ClientInformation::GetAccount()
    {
        return m_account;
    }

    const std::string ClientInformation::GetPassword()
    {
        return m_password;
    }

    const std::string ClientInformation::GetNickname()
    {
        return m_nickname;
    }

    void ClientInformation::OnPack(sf::Packet& Packet)
    {
        Packet << m_account << m_password << m_nickname;
    }

    void ClientInformation::OnUnpack(sf::Packet& Packet)
    {
        Packet >> m_account >> m_password >> m_nickname;
    }

    void ClientInformation::RegisterType()
    {
        stlr::PackableTypes::RegisterType(stlr::ClientInformation::ClientInformationTypeID, [](PackablePtr& ConstructedTransferable) { ConstructedTransferable.reset(new stlr::ClientInformation("", "", "")); });
    }
}

#endif