#ifndef STLR_LOCKABLEBASE_CPP
#define STLR_LOCKABLEBASE_CPP

#include <Streamliner/LockableBase.hpp>

namespace stlr
{
    LockableBase::LockableBase() : m_lock(), m_count(0) { }

    void LockableBase::Lock()
    {
        m_count += 1;
        m_lock.lock();
    }

    void LockableBase::Release()
    {
        m_count -= 1;
        m_lock.unlock();
    }

    const bool LockableBase::IsLocked()
    {
        return m_count > 0;
    }
}

#endif
