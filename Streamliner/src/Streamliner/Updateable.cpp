#ifndef STLR_UPDATEABLE_CPP
#define STLR_UPDATEABLE_CPP

#include <Streamliner/Updateable.hpp>

namespace stlr
{
    Updateable::Updateable(PackableTypeID TypeID) : stlr::Packable(TypeID), m_remoteupdated(false) { }

    const bool Updateable::RemoteUpdateAvailable()
    {
        return m_remoteupdated;
    }

    void Updateable::SetRemoteUpdateAvailable()
    {
        m_remoteupdated = true;
    }

    void Updateable::RemoteUpdateHandled()
    {
        m_remoteupdated = false;
    }
}

#endif
