#ifndef STLR_PACKABLE_CPP
#define STLR_PACKABLE_CPP

#include <Streamliner/Packable.hpp>

namespace stlr
{
    Packable::Packable(PackableTypeID TypeID) : m_typeid(TypeID) { }

    void Packable::Pack(sf::Packet& Packet)
    {
        OnPack(Packet);
    }

    void Packable::Unpack(sf::Packet& Packet)
    {
        OnUnpack(Packet);
    }

    sf::Packet& Packable::operator >>(sf::Packet& Packet)
    {
        Unpack(Packet);
        return Packet;
    }

    sf::Packet& Packable::operator <<(sf::Packet& Packet)
    {
        Pack(Packet);
        return Packet;
    }
}

#endif
