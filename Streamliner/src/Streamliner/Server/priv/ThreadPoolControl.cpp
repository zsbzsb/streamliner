#ifndef PRIV_THREADCONTROL_CPP
#define PRIV_THREADCONTROL_CPP

#include <Streamliner/Server/priv/ThreadPoolControl.hpp>

namespace priv
{
    ThreadPoolControl::ThreadPoolControl() : ShouldRun(), RunningCount(), JobQueue()
    {
        ShouldRun = false;
        RunningCount = 0;
    }
}

#endif
