#ifndef PRIV_WORKERTHREAD_CPP
#define PRIV_WORKERTHREAD_CPP

#include <Streamliner/Server/priv/WorkerThread.hpp>

namespace priv
{
    WorkerThread::WorkerThread(priv::ThreadPoolControl* Control) : m_control(Control), m_thread() { }

    void WorkerThread::Start()
    {
        m_control->RunningCount += 1;
        m_thread.reset(new std::thread(&WorkerThread::ThreadRun, this));
    }

    void WorkerThread::ThreadRun()
    {
        while (m_control->ShouldRun)
        {
            auto& queue = m_control->JobQueue.LockItem();
            auto worker = FindWork(queue);
            m_control->JobQueue.Release();
            if (worker)
            {
                int iterations = 0;
                while (worker->DoesNeedWork() && iterations < 3)
                {
                    worker->DoWork();
                    iterations += 1;
                }
                auto& queue = m_control->JobQueue.LockItem();
                queue.push(worker);
                worker.reset();
                m_control->JobQueue.Release();
            }
            else std::this_thread::sleep_for(std::chrono::milliseconds(5));
        }
        m_control->RunningCount -= 1;
    }

    JobSharedPtr WorkerThread::FindWork(std::queue<JobWeakPtr>& JobQueue)
    {
        unsigned int minimum = 0;
        while (JobQueue.size() > minimum)
        {
            auto worker = JobQueue.front().lock();
            JobQueue.pop();
            if (worker && worker->DoesNeedWork()) return worker;
            else JobQueue.push(worker);
            worker.reset();
            minimum += 1;
        }
        return nullptr;
    }

    void WorkerThread::Join()
    {
        if (m_thread->joinable()) m_thread->join();
    }
}

#endif
