#ifndef STLR_CLIENT_CPP
#define STLR_CLIENT_CPP

#include <Streamliner/Server/Client.hpp>

namespace stlr
{
    Client::Client() : m_socket(), m_information(), m_localupdateables(), m_remoteupdateables(), m_receivedpackables(), m_custompackets() { }

    Client::~Client()
    {
        m_socket.disconnect();
    }

    sf::TcpSocket& Client::GetSocket()
    {
        return m_socket;
    }

    const PackablePtr& Client::GetInformation()
    {
        return m_information;
    }

    void Client::HandleIncomingData()
    {
        PacketPtr packet(new sf::Packet);
        while (true)
        {
            auto result = m_socket.receive(*packet);
            if (result == sf::Socket::Done)
            {
                PacketType packettype;
                *packet >> packettype;
                switch (packettype)
                {
                    case 0: // customer information
                    {
                        PackableTypeID packabletype;
                        *packet >> packabletype;
                        PackableTypes::ConstructPackable(packabletype, m_information);
                        m_information->Unpack(*packet);
                        break;
                    }
                    case 1: // authorization state
                    {
                        // well if the client tells us this then something is screwy....
                        // why would the client be authorizing the server???
                        break;
                    }
                    case 2: // custom data
                    {
                        m_custompackets.push(std::move(packet));
                        packet.reset(new sf::Packet);
                        break;
                    }
                    case 3: // packable
                    {
                        PackableTypeID packabletype;
                        *packet >> packabletype;
                        PackablePtr packable;
                        PackableTypes::ConstructPackable(packabletype, packable);
                        m_receivedpackables.push(std::move(packable));
                        break;
                    }
                    case 4: // create updateable
                    {
                        PackableTypeID packabletype;
                        UpdateableID updateableid;
                        *packet >> packabletype >> updateableid;
                        PackablePtr packable;
                        PackableTypes::ConstructPackable(packabletype, packable);
                        m_remoteupdateables.insert({ updateableid, UpdateablePtr(dynamic_cast<Updateable*>(packable.release())) });
                        break;
                    }
                    case 5: // update updateable
                    {
                        UpdateableID updateableid;
                        *packet >> updateableid;
                        m_remoteupdateables[updateableid]->Unpack(*packet);
                        m_remoteupdateables[updateableid]->SetRemoteUpdateAvailable();
                        break;
                    }
                    case 6: // drop updateable
                    {
                        UpdateableID updateableid;
                        *packet >> updateableid;
                        Updateable* updateable = m_remoteupdateables[updateableid].get();
                        for (auto& mapped : m_mappedupdateables)
                        {
                            if (mapped.second == updateable)
                            {
                                m_mappedupdateables.erase(mapped.first);
                                break;
                            }
                        }
                        m_remoteupdateables.erase(updateableid);
                        break;
                    }
                    default:
                        break;
                }
            }
            else if (result == sf::Socket::NotReady) break;
            else
            {
                // disconnected
            }
        }
    }

    void Client::SendAuthorizedState(bool Authorized)
    {
        sf::Packet packet;
        PacketType type = 1;
        packet << type << Authorized;
        m_socket.send(packet);
    }

    const bool Client::HandleRecievedPackable(PackablePtr& TransferableOut)
    {
        if (m_receivedpackables.size() > 0)
        {
            std::swap(m_receivedpackables.front(), TransferableOut);
            m_receivedpackables.pop();
            return true;
        }
        return false;
    }

    const PackablePtr& Client::PeakRecievedPackable()
    {
        return m_receivedpackables.front();
    }

    const size_t Client::GetRecievedPackableCount()
    {
        return m_receivedpackables.size();
    }

    void Client::SkipRecievedPackable()
    {
        PackablePtr packable;
        std::swap(m_receivedpackables.front(), packable);
        m_receivedpackables.pop();
        m_receivedpackables.push(std::move(packable));
    }

    const bool Client::HandleCustomPacket(PacketPtr& PacketOut)
    {
        if (m_custompackets.size() > 0)
        {
            std::swap(m_custompackets.front(), PacketOut);
            m_custompackets.pop();
            return true;
        }
        return false;
    }

    const PacketPtr& Client::PeakCustomPacket()
    {
        return m_custompackets.front();
    }

    const size_t Client::GetCustomPacketCount()
    {
        return m_custompackets.size();
    }

    void Client::SkipCustomPacket()
    {
        PacketPtr packet;
        std::swap(m_custompackets.front(), packet);
        m_custompackets.pop();
        m_custompackets.push(std::move(packet));
    }

    const size_t Client::GetLocalUpdateablesCount()
    {
        return m_localupdateables.size();
    }

    const UpdateablePtr& Client::GetLocalUpdateable(size_t Index)
    {
        assert(Index < m_localupdateables.size());
        auto& iterator = m_localupdateables.begin();
        std::advance(iterator, Index);
        return iterator->second;
    }

    const size_t Client::GetRemoteUpdateablesCount()
    {
        return m_remoteupdateables.size();
    }

    const UpdateablePtr& Client::GetRemoteUpdateable(size_t Index)
    {
        assert(Index < m_remoteupdateables.size());
        auto& iterator = m_remoteupdateables.begin();
        std::advance(iterator, Index);
        return iterator->second;
    }
}

#endif
