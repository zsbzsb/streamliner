#ifndef STLR_CLIENTAUTHORIZEWORKER_CPP
#define STLR_CLIENTAUTHORIZEWORKER_CPP

#include <Streamliner/Server/ClientAuthorizeJob.hpp>

namespace stlr
{
    ClientAuthorizeJob::ClientAuthorizeJob() : m_updateclock(), m_sourcepools(), m_destinationpools(), m_clientauthorizationfunction(stlr::ClientAuthorizeJob::DefaultClientAuthorization), m_assignpoolfunction(&stlr::ClientAuthorizeJob::DefaultAssignPool) { }

    void ClientAuthorizeJob::RegisterSourcePool(ClientPoolPtr ClientPool)
    {
        m_sourcepools.push_back(ClientPool);
    }

    void ClientAuthorizeJob::RegisterDestinationPool(ClientPoolPtr ClientPool)
    {
        m_destinationpools.push_back(ClientPool);
    }

    void ClientAuthorizeJob::Initialize()
    {
        m_updateclock.restart();
    }

    const bool ClientAuthorizeJob::DoesNeedWork()
    {
        return m_updateclock.getElapsedTime().asSeconds() >= .25f;
    }

    void ClientAuthorizeJob::DoWork()
    {
        m_updateclock.restart();
        for (auto& pool : m_sourcepools)
        {
            if (pool->GetClientCount() > 0)
            {
                auto& clients = pool->GetClients().LockItem();
                for (unsigned int i = 0; i < clients.size();)
                {
                    ClientPtr client = clients[i];
                    client->Lock();
                    if (client->GetInformation())
                    {
                        pool->RemoveClient(client);
                        if (m_clientauthorizationfunction(client))
                        {
                            client->SendAuthorizedState(true);
                            m_assignpoolfunction(client, m_destinationpools);
                        }
                        else
                        {
                            client->SendAuthorizedState(false);
                        }
                    }
                    client->Release();
                }
                pool->GetClients().Release();
            }
        }
    }

    void ClientAuthorizeJob::SetClientAuthorizationFunction(std::function<const bool(ClientPtr&)> Function)
    {
        m_clientauthorizationfunction = Function;
    }

    void ClientAuthorizeJob::SetAssignPoolFunction(std::function<void(ClientPtr& Client, const std::vector<ClientPoolPtr>&)> Function)
    {
        m_assignpoolfunction = Function;
    }

    const bool ClientAuthorizeJob::DefaultClientAuthorization(ClientPtr& Client)
    {
        return true;
    }

    void ClientAuthorizeJob::DefaultAssignPool(ClientPtr& Client, const std::vector<ClientPoolPtr>& Pools)
    {
        if (Pools.size() == 1)
        {
            Pools[0]->AddClient(Client);
        }
        else if (Pools.size() > 1)
        {
            auto selectedpool = Pools[0];
            for (auto& pool : Pools)
            {
                if (pool->GetClientCount() < selectedpool->GetClientCount()) selectedpool = pool;
            }
            selectedpool->AddClient(Client);
        }
    }
}

#endif
