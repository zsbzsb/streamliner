#ifndef STLR_SERVER_CPP
#define STLR_SERVER_CPP

#include <Streamliner/Server/Server.hpp>

namespace stlr
{
    Server::Server(unsigned short WorkerCount) : m_threadcontrol(), m_workerthreads(), m_jobs()
    {
        for (unsigned short i = 0; i < WorkerCount; i++)
        {
            m_workerthreads.push_back(WorkerThreadPtr(new priv::WorkerThread(&m_threadcontrol)));
        }
    }

    void Server::RegisterJob(JobSharedPtr& Job)
    {
        m_jobs.push_back(Job);
    }

    void Server::Start()
    {
        m_threadcontrol.ShouldRun = true;
        auto& queue = m_threadcontrol.JobQueue.LockItem();
        for (auto& job : m_jobs)
        {
            job->Initialize();
            queue.push(job);
        }
        m_threadcontrol.JobQueue.Release();
        for (auto& workerthread : m_workerthreads)
        {
            workerthread->Start();
        }
    }

    void Server::Stop()
    {
        m_threadcontrol.ShouldRun = false;
        for (auto& worker : m_workerthreads)
        {
            worker->Join();
        }
        for (auto& job : m_jobs)
        {
            job->Shutdown();
        }
    }
}

#endif
