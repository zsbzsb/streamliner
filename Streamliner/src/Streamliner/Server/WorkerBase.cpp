#ifndef PRIV_WORKERBASE_CPP
#define PRIV_WORKERBASE_CPP

#include <Streamliner/Server/JobBase.hpp>

namespace stlr
{
    JobBase::~JobBase() { }

    void JobBase::Initialize() { }

    void JobBase::Shutdown() { }
}

#endif
