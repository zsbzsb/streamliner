#ifndef STLR_CLIENTPOOL_CPP
#define STLR_CLIENTPOOL_CPP

#include <Streamliner/Server/ClientPool.hpp>

namespace stlr
{
    ClientPool::ClientPool() : m_clientcount(0), m_clients() { }

    void ClientPool::AddClient(ClientPtr& Client)
    {
        m_clientcount += 1;
        auto& clients = m_clients.LockItem();
        clients.push_back(Client);
        m_clients.Release();
    }

    void ClientPool::RemoveClient(ClientPtr& Client)
    {
        auto& clients = m_clients.LockItem();
        for (auto& client : clients)
        {
            if (client == Client)
            {
                m_clientcount -= 1;
                std::swap(client, clients.back());
                clients.pop_back();
                break;
            }
        }
        m_clients.Release();
    }

    void ClientPool::MoveClient(ClientPtr& Client, std::shared_ptr<stlr::ClientPool>& ClientPool)
    {
        RemoveClient(Client);
        ClientPool->AddClient(Client);
    }

    unsigned short ClientPool::GetClientCount()
    {
        return m_clientcount;
    }

    stlr::LockableItem<std::deque<ClientPtr>>& ClientPool::GetClients()
    {
        return m_clients;
    }
}

#endif
