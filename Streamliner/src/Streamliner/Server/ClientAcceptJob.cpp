#ifndef STLR_CLIENTACCEPTWORKER_CPP
#define STLR_CLIENTACCEPTWORKER_CPP

#include <Streamliner/Server/ClientAcceptJob.hpp>

namespace stlr
{
    ClientAcceptJob::ClientAcceptJob(unsigned short Port, PackableTypeID ClientInformationTypeID) : m_listener(), m_port(Port), m_updateclock(), m_clientpools(), m_infotypeid(ClientInformationTypeID)
    {
        m_listener.setBlocking(false);
    }

    void ClientAcceptJob::RegisterClientPool(ClientPoolPtr ClientPool)
    {
        m_clientpools.push_back(ClientPool);
    }

    void ClientAcceptJob::Initialize()
    {
        m_listener.listen(m_port);
        m_updateclock.restart();
    }

    const bool ClientAcceptJob::DoesNeedWork()
    {
        return m_updateclock.getElapsedTime().asSeconds() >= .25f;
    }

    void ClientAcceptJob::DoWork()
    {
        m_updateclock.restart();
        while (true)
        {
            ClientPtr client(new Client());
            if (m_listener.accept(client->GetSocket()) == sf::Socket::Done)
            {
                client->GetSocket().setBlocking(false);
                if (m_clientpools.size() == 1)
                {
                    m_clientpools[0]->AddClient(client);
                }
                else if (m_clientpools.size() > 1)
                {
                    auto& selectedpool = m_clientpools[0];
                    for (auto& pool : m_clientpools)
                    {
                        if (pool->GetClientCount() < selectedpool->GetClientCount()) selectedpool = pool;
                    }
                    selectedpool->AddClient(client);
                }
                else client->SendAuthorizedState(false);
            }
            else break;
        }
    }

    void ClientAcceptJob::Shutdown()
    {
        m_listener.close();
    }
}

#endif
