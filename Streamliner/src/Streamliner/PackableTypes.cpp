#ifndef STLR_PACKABLETYPES_CPP
#define STLR_PACKABLETYPES_CPP

#include <Streamliner/PackableTypes.hpp>

namespace stlr
{
    std::unordered_map<PackableTypeID, std::function<void(PackablePtr&)>> PackableTypes::m_types;

    void PackableTypes::RegisterType(PackableTypeID TypeID, std::function<void(PackablePtr&)> Contructor)
    {
        m_types.insert({ TypeID, Contructor });
    }

    void PackableTypes::ConstructPackable(PackableTypeID TypeID, PackablePtr& ConstructedPackable)
    {
        m_types[TypeID](ConstructedPackable);
    }
}

#endif
